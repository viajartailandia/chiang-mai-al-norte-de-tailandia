# Chiang Mai Tailandia #

**Chiang Mai es una ciudad situada al norte de Tailandia en el Sudeste Asiático**, destaca su religión budista y la cultura y simpatia de su gente, también es conocida como la capital de los nomadas digitales y todo aquel que desea vivir en el país asiático gracias a sus negocios online es bienvenido, conoce infinidad de templos y más información interesante en [**este artículo sobre Chiang Mai**](https://todotailandia.com/chiang-mai/) no te pierdas nada del turismo en Tailandia y disfruta al máximo de tus vacaciones por libre en el reino de Siam.

## Que ver en Chiang Mai ##

* Templos
* Bazares nocturnos
* Casco histórico
* Hacer trekking
* Elefantes y tigres en reservas protegidas